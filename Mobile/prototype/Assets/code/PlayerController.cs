﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    float speed = 9;

    [SerializeField, Tooltip("Acceleration while grounded.")]
    float walkAcceleration = 75;

    [SerializeField, Tooltip("Acceleration while in the air.")]
    float airAcceleration = 30;

    [SerializeField, Tooltip("Deceleration applied when character is grounded and not attempting to move.")]
    float groundDeceleration = 70;

    [SerializeField, Tooltip("Max height the character will jump regardless of gravity")]
    float jumpHeight = 4;

    [SerializeField]
    Vector2 WallJumpHeigth = new Vector2(0,2);
    [SerializeField]
    float WallSlide = 2;

    [SerializeField]
    private float DashSpeed = 1;


    private Vector2 velocity;
    [SerializeField]
    private bool grounded;

    [SerializeField]
    private float G_mutiple = 0;

    private int Direction = 0;

    private Rigidbody2D rb;
  
    [SerializeField]
  private  BoxCollider2D L_Wall;
  
    [SerializeField]
    private BoxCollider2D R_Wall;
    [SerializeField]
    private bool IsWall;
    [SerializeField]
    private LayerMask WallLayer;


    [SerializeField]
    private BoxCollider2D GroundCheck;
   
    [SerializeField]
    private LayerMask GroundLayer;

    private int Wall_LR=0;

    [SerializeField]
   private bool isdead=false;

    private bool IsSlid = false;

    [SerializeField]
    private bool Dash = false;

    private float i = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }
    
    // Update is called once per frame
    void Update()
    {
        Debug.Log(isdead);
       
        transform.Translate(velocity * Time.deltaTime);

        if (!isdead) {
            
           
                float moveInput = Input.GetAxisRaw("Horizontal");
        velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, walkAcceleration * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.E)&&!Dash)
            {
                if (i <= 0)
                {
                    i = 0.1f;
                    rb.velocity = Vector2.zero;
                    
                   
                }
                else
                {
                    Dash = true;
                    i = i - Time.deltaTime;
                    DashOn();
                }

                

                //for (i=0; i <= 1f;i+=Time.deltaTime)
                //{
                //Dash = true;
                //DashOn();
                //Debug.Log("I"+i);
                //}
                //Dash = false;
            }else if (Input.GetKeyUp(KeyCode.E) && Dash)
            {
                Dash = false;
            }




                if (moveInput > 0)
                Direction = 1;
          else  if (moveInput < 0)
                Direction = -1;
           

            float acceleration = grounded ? walkAcceleration : airAcceleration;
        float deceleration = grounded ? groundDeceleration : 0;
          
                if (moveInput != 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, acceleration * Time.deltaTime);
        }
        else
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
        }



        if(IsWall)
            {
   
             if (!grounded&& Input.GetButtonDown("Jump"))
             {
           velocity = new Vector2(speed*Wall_LR, jumpHeight)* WallJumpHeigth;
                IsSlid = false;

            }

             if (!grounded&&velocity.y<0)
              {
                IsSlid = true;
                velocity.y = WallSlide;
               }
     
            }

    
          grounded= (BoxCollider2D)Physics2D.OverlapBox(GroundCheck.transform.position,GroundCheck.size, 0, GroundLayer);
            bool left = (BoxCollider2D)Physics2D.OverlapBox(L_Wall.transform.position,L_Wall.size , 0, WallLayer);
        bool rigth= (BoxCollider2D)Physics2D.OverlapBox(R_Wall.transform.position, R_Wall.size, 0, WallLayer);
        IsWall = left|| rigth;

        if (left)
            Wall_LR = 1;
        if (rigth)
            Wall_LR = -1;


            if (grounded)
            {
                IsSlid = false;
                velocity.y = 0;

                // Jumping code we implemented earlier—no changes were made here.
                if (Input.GetButtonDown("Jump"))
                {
                    // Calculate the velocity required to achieve the target jump height.
                     velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
               //  velocity = new Vector2(velocity.x, jumpHeight);


                }

            }
             else
            {
          
                 velocity.y += Physics2D.gravity.y*G_mutiple *Time.deltaTime;
                 Debug.Log(velocity.y);
            }
        
     
    }

        }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            isdead = true;
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(L_Wall.transform.position, L_Wall.size);

        Gizmos.color = Color.red;
        Gizmos.DrawCube(R_Wall.transform.position, R_Wall.size);

        Gizmos.color = Color.blue;
        Gizmos.DrawCube(GroundCheck.transform.position, GroundCheck.size);
    }

    void DashOn()
    {
        if (Dash) { 
           if (Direction==1)
            {
                rb.velocity = (Vector2.right * DashSpeed);
            }
            if (Direction == -1)
            {
                rb.velocity=(Vector2.left * DashSpeed);
            }
        }
    }
}
