﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    private bool isDead = false;

    [SerializeField]
    public static GameManager Insatance;
    public Vector3 Pos;
    // Start is called before the first frame update
    void Start()
    {
        Insatance = this;   
    }

    // Update is called once per frame
    void Update()
    {
        if(!isDead&&Player.transform.position.y<-50)
        {
            isDead = true;

        }
        if (isDead)
        {
            Player.transform.position = new Vector2(Pos.x, Pos.y);
            isDead = false;
        }
               
        
    }
}
