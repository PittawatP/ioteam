﻿
using UnityEngine;

public class ItemPickup : Interactable
{
    public Item item;
    public override void Interact()
    {

        base.Interact();
        
        PickUp();
    }
    void PickUp()
    {
      
        Debug.Log("picking up item"+item.name);
     bool wasPickup=   Inventory.instance.Add(item);
       
        if (wasPickup) 
        Destroy(gameObject);
    }
}
