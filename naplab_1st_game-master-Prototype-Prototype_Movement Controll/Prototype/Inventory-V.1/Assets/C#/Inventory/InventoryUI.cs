﻿
using UnityEngine;

public class InventoryUI : MonoBehaviour
{  
    public Transform itenParent;
    public GameObject InventoryUi;

    Inventory inventory;
    InventorySlot[] slot;
    // Start is called before the first frame update
    void Start()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
        slot = itenParent.GetComponentsInChildren<InventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Inventory"))
        {
            InventoryUi.SetActive(!InventoryUi.activeSelf);
        }
    }
    void UpdateUI()
    {
        Debug.Log("Updatting UI");
       for(int i=0;i<slot.Length;i++)
        {
            if(i<inventory.items.Count)
            {
                slot[i].AddItem(inventory.items[i]);
            }else
            {
                slot[i].ClearSlot();
            }
        }
    }


}
