﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : ChartrecterStat
{
    // Start is called before the first frame update
    void Start()
    {
        EquipmentManeger.instance.onEquipmentChanged += OnEquipmentChanged;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnEquipmentChanged(Equipment newItem,Equipment oldItem)
    {
        if(newItem!=null)
        {

        arrmor.AddModifier(newItem.armorModifier);
        damage.AddModifier(newItem.damageModifier);
        }
        if (oldItem != null)
        {

            arrmor.RemoveModifier(newItem.armorModifier);
            damage.RemoveModifier(newItem.damageModifier);
        }

    }
}
