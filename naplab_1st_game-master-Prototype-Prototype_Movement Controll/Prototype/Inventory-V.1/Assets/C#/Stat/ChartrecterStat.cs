﻿
using UnityEngine;

public class ChartrecterStat : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth { get; private set;} 
    
    public Stat damage;
    public Stat arrmor;
 void Awake()
{
    currentHealth = maxHealth;
}


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            takeDamage(10);
        }
    }
    public void takeDamage(int damage)
    {
        damage -= arrmor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);
        currentHealth -= damage;
        Debug.Log(transform.name + " take " + damage + " damage ");

        if(currentHealth<=0)
        {
            Die();
        }

    }

    public virtual void Die()
    {

        Debug.Log("Die");
    }
        

}
