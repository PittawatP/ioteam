﻿using UnityEngine.EventSystems;
using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    public LayerMask movementMask;
    Camera cam;
    PlayerMotor motor;
    public Interactable Focus;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        motor = GetComponent<PlayerMotor>();
    }

    // Update is called once per frame
    void Update()
    {
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray,out hit,100,movementMask))
            {
                motor.MoveToPoint(hit.point);
                RemoveFocus();
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Interactable interact = hit.collider.GetComponent<Interactable>();
                if(interact!=null)
                {
                    SetFocus(interact);
                }
            }
        }

    }

    void SetFocus (Interactable newFocus)
    {
        if (newFocus != Focus)
        {
            if(Focus!=null)
            {
                Focus.OnDefousde();
            }
         Focus = newFocus;
         motor.FllowTarget(newFocus);
        }
         newFocus.OnFocused(transform);
    }
    void RemoveFocus( )
    {   if(Focus!=null)
        Focus.OnDefousde();


        Focus = null;
        motor.StopFllowTarget();
    }


}
