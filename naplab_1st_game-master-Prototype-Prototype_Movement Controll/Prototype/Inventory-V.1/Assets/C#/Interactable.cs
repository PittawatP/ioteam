﻿
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public Transform interactionTranform;

    bool isFocuc = false;
    Transform player;
    bool hasInteract = false;
    public virtual void Interact()
    {
        Debug.Log("interact");
    }

    private void Update()
    {
        if(isFocuc&&!hasInteract)
        {
            float distabe = Vector3.Distance(player.position, interactionTranform.position);
            if(distabe<=radius)
            {
                Interact();
               
                hasInteract = true;
            }
           
        }
    }
    public void OnFocused(Transform  playerTarnsform)
    {
        isFocuc = true;
        player = playerTarnsform; 
        hasInteract = false;
    }
    public void OnDefousde()
    {
        isFocuc = false;
        player = null;
        hasInteract = false;
    }
    void OnDrawGizmosSelected()
    {
        if (interactionTranform == null)
        {
            interactionTranform = transform;
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTranform.position, radius);

    }

}
