﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSpawn : MonoBehaviour
{   [SerializeField]
    private List<GameObject> Prefabs_monster;
    [SerializeField]
    private List<GameObject> PositionSpawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i =0;i<Prefabs_monster.Count;i++)
        {
            Instantiate(Prefabs_monster[i], PositionSpawn[i].transform.position,Quaternion.identity);
        }
        
    }
}
