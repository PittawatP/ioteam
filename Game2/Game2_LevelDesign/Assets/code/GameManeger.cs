﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManeger : MonoBehaviour
{
    [SerializeField]
    public int HPPlayer = 100;

    private int Score { get; set; }

    [SerializeField]
    public int Temperature=1;
    //G Y O R

    [SerializeField]
    public int Ammo { get; set; }

    public static GameManeger instant;

    public bool isFaceOn = false;
    public int Faceprotect = 3;

    [SerializeField]
    public int Pill { get; set; }

    private Vector3 SavePoint;
    // Start is called before the first frame update
    void Start()
    {
        instant = this;
        Ammo = 10;
        Pill = 1;
    }

    // Update is called once per frame
    void Update()
    {
     

        
        if(HPPlayer>=100)
        { HPPlayer = 100; 
        }

        if (Temperature <= 1)
            Temperature = 1;
        if (Temperature >= 4)
            Temperature = 4;

        if(Pill<0)
        { Pill = 0; }
    }
}
