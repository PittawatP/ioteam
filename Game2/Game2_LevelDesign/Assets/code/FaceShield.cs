﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceShield : MonoBehaviour
{ 

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            GameManeger.instant.Faceprotect = 3;
            GameManeger.instant.isFaceOn = true;
            Destroy(gameObject);
        }
    }
}
