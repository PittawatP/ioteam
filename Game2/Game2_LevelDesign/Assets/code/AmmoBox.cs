﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    [SerializeField]
    private int PlusAmmo = 20;

    private void OnCollisionEnter(Collision collision)
    {
         if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            GameManeger.instant.Ammo += PlusAmmo;
        }
    }
  
}
