﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Control : MonoBehaviour
{
   
    public Rigidbody rb;
    Vector3 movement;


    public GameObject bullet;
     public const float ANGLE_STEP = 3;
    float _currentAngle = 90;
    float _length = 1.5f;
    public Camera cam;
    public int direction = 0;
    Vector3 worldPosition;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
            direction = 1;
        if (Input.GetKeyDown(KeyCode.A))
            direction = 0;


        Vector3 mousePos = Input.mousePosition;
        mousePos.z = cam.nearClipPlane;
        worldPosition = cam.ScreenToWorldPoint(mousePos);
        Debug.Log(worldPosition);

        float sine = Mathf.Sin(_currentAngle * Mathf.Deg2Rad);
        float cosine = Mathf.Cos(_currentAngle * Mathf.Deg2Rad);
        Vector3 unitCircleDirection = new Vector3(_length * cosine, _length * sine, 0);
       bullet.transform.position = unitCircleDirection + transform.position;



        Vector3 lookDir = worldPosition - Vector3.zero;
        //Debug.Log(lookDir);

        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        _currentAngle = angle;

        //bullet.transform.rotation = Quaternion.Euler(0,0,angle);
        if (Input.GetMouseButtonDown(0)) {
            Vector3 Direction = new Vector3(lookDir.x - rb.position.x, lookDir.y - rb.position.y, rb.position.z);
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            
            go.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            SphereCollider coli = go.GetComponent<SphereCollider>();
            coli.isTrigger = true;
            Rigidbody Rb = go.AddComponent<Rigidbody>();
            Rb.useGravity = false;
            go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
           Rb.gameObject.tag = "Bullet";
            Rb.AddForce(Direction.normalized, ForceMode.Impulse);
            Destroy(go, 2f);
        }
     

    }
    
   
}
