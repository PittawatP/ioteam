﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour
{

    public Camera cam;
    Vector3 worldPosition;
    public GameObject ball;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

     //  if (Input.GetKey(KeyCode.UpArrow) && cam.nearClipPlane>=0)
     //  {
     //      cam.nearClipPlane++;
     //  }
     //  if (Input.GetKey(KeyCode.DownArrow) && cam.nearClipPlane>=0)
     //  {
     //      cam.nearClipPlane--;
     //  }

        //  mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        //  Debug.Log(mousePos);
        worldPosition.z = transform.position.z;
        ball.transform.position =  transform.position+worldPosition;
       
       Vector3 mousePos = Input.mousePosition;
       mousePos.z = Camera.main.nearClipPlane;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
        Debug.Log(worldPosition);


        Vector3 lookDir = ball.transform.position - transform.position;
        if (Input.GetMouseButtonDown(0)&&GameManeger.instant.Ammo>0)
        {
            GameManeger.instant.Ammo--;
               GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            go.gameObject.tag = "Bullet";
            SphereCollider coli = go.GetComponent<SphereCollider>();
            coli.isTrigger = true;
            Rigidbody Rb = go.AddComponent<Rigidbody>();
            Rb.useGravity = false;
            go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
            go.AddComponent<Bullet>();
            Rb.AddForce(lookDir.normalized*Speed, ForceMode.Impulse);
            Destroy(go, 2f);
        }
    }
}
