﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField]
    private int HP=4;
    [SerializeField]
    private Vector3 Movement;
    [SerializeField]
    private float Speed;
    private Rigidbody rb;
    [SerializeField]
    private int MaxRange=5;

    private Vector3 Pos;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
       

        if (HP <= 0)
        { Destroy(gameObject); }
           


       
    }
    private void FixedUpdate()
    {
        move();
    }

    public void move()
    {
        rb.MovePosition(rb.position + Movement * Speed * Time.deltaTime);
       
        if(rb.position.x>Pos.x+MaxRange||rb.position.x<Pos.x-MaxRange)
        {
            Movement = -Movement;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            HP--;
        }
    }

    
}
