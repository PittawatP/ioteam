﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    private float speed;
    public GameObject Prefabs;
    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
       
        Vector3 Movement = new Vector3(h, 0, 0);
        rb.MovePosition(rb.position+Movement*speed*Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up*speed, ForceMode.Impulse);
        }

        if(Input.GetKeyDown(KeyCode.Q)&&GameManeger.instant.Pill>0)
        {
            GameManeger.instant.HPPlayer += 25;
            GameManeger.instant.Temperature--;

            GameManeger.instant.Pill--;
        }
        if (GameManeger.instant.isFaceOn)
        {
            Prefabs.SetActive(true);
        }
        else Prefabs.SetActive(false);


        if(GameManeger.instant.HPPlayer<=0)
        { Destroy(gameObject); }
    }
}
